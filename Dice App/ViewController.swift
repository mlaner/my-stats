//
//  ViewController.swift
//  Dice App
//
//  Created by Myles Lane on 5/11/16.
//  Copyright © 2016 Myles Lane. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let diceArray : [String] = ["dice1", "dice2", "dice3", "dice4", "dice5", "dice6" ]
    
    
    @IBOutlet weak var diceImageView1: UIImageView!
    @IBOutlet weak var diceImageView2: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
       changeDiceFace()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Roll Button Method
    @IBAction func rollButtonPressed(sender: UIButton) {
        
        print("roll button was pressed")
        changeDiceFace()
    }
    
    func changeDiceFace() {
        
        let firstRandomIndex = Int(arc4random_uniform(6))
        let secondRandomIndex = Int(arc4random_uniform(6))
        
        diceImageView1.image = UIImage(named: diceArray[firstRandomIndex])
        diceImageView2.image = UIImage(named: diceArray[secondRandomIndex])
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        changeDiceFace()
    }
}



